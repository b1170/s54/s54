let collection = [];

// Write the queue functions below.

function print() {
    return collection
}

function enqueue(x){
   collection[collection.length] = x
   return collection
}

function dequeue(x){
    let collectionIndex = collection.indexOf(x)
    collection = collection.splice(collectionIndex,1)
    return collection
}

function front(){
    return collection[0]

}

function size(){
    return collection.length
}


function isEmpty(){
    if(collection.length != 0){
        return false
    } else {
        return true
    }
}


module.exports = {
       print,
       enqueue,
       dequeue,
       front,
       size,
       isEmpty
};